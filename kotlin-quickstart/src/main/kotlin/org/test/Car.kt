package org.test


data class Car(var height: Double, var length: Double = 4.9){
    val name = "height: ${height}, length $length"
    val computed:String by lazy {
        "this is a computed prop height: ${height}, length $length"
    }
    init{

    }


}