package org.test

import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/hello")
class GreetingResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    fun hello():String {
        val additional = changeCar()
        return SomeJava.getSomeMsg() + " " + additional
    } 


    fun sum(a: Int, b: Int) = a + b

    fun changeCar():String {
        val car = Car(4.0, 7.0)
        car.height = 3.2
        println(car)

        val x = listOf(1, 2, 3)


        for (x in 1..5) {
            println(x)
        }

        return car.computed
    }


}